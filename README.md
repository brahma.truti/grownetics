# Grownetics

Welcome to Grownetics Open Source repository.

Grownetics is a platform to help the world learn how to grow every plant better.

This repository will contain as much as we can possibly open source.

## Repository Status

Until recently all of our source was closed. We are moving to open as much of it as we can. As we do that, there will be problems, bugs, etc. This repository can be considered unstable.

# Handbook

[Grownetics Company Handbook](http://docs.grownetics.co/)

[Using our Documentation](http://docs.grownetics.co/documentation/)

# Technical Documentation

* [GrowServer](http://docs.grownetics.co/Development/growserver/)
* [Firmware](http://docs.grownetics.co/Development/firmware/)
* [GrowCtl](http://docs.grownetics.co/Development/growctl/)

# Reporting bugs

If you find a bug with the documentation, or something that could be improved, please open an [issue](https://code.cropcircle.io/Grownetics/Grownetics/issues).

# Community

Please join us and let us know what you think!

## [Chat](https://grownetics.zulipchat.com/)
## [Reddit](https://reddit.com/r/Grownetics/)
## [Twitch](https://www.twitch.tv/nickbusey/)
