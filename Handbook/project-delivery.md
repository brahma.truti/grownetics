# Project Delivery

## Lead Intake 

### Intake Funnel


### Lead Folder and Integration Plan Setup

Create new lead folder and subdirectory with client drawings, upload client drawings from email.

For example: in Google Drive All Company/Leads/Lead Name/Client Drawings. 

**Note: Some clients require a Mutual NDA to be signed first**

If the client would like a mutual nda generated we can do this easily with our [Hello Sign Templates](https://hellosign.com)

#### Integration Plan

The integration plan is made up of an Adobe Illustrator .ai file which is a rudimentary dashboard map (see how to make dashborad maps in the UI/UX docs. [link](http://docs.grownetics.co/Development/ux/)). From this we generate .png's to insert into the integration plan google slide template. Which is then used to generate the final pdf for the client.

Duplicate the most recent sensor reference map from a previous lead folder and rename to [Client Name - Sensor Ref Map.ai]

## Sourcing Vendors and Subcontractors

If we don't have a preferred vendor for a specific work functioin always source at least 3, get 3+ opinions, or quotes/bids.






